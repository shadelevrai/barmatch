import React, { useEffect, useState } from "react";
import { Text, View } from "react-native";
import { firebase } from "../../firebaseConfig";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import PresentationLogoScreen from "../../Screens/PresentationLogo";
import LoginScreen from "../../Screens/Login";
import HomeMapScreen from "../../Screens/HomeMap";
import ResetPasswordScreen from "../../Screens/ResetPassword";
import ProfileScreen from "../../Screens/Profile";

const Stack = createStackNavigator();

export default function NavigatorComponent() {
  const [user, setUser] = useState(false);

  useEffect(() => {
    firebaseGetUser();
  }, []);

  function firebaseGetUser() {
    const usersRef = firebase.firestore().collection("users");
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        usersRef
          .doc(user.uid)
          .get()
          .then((document) => {
            const userData = document.data();
            setUser(userData);
          })
          .catch((error) => {
            console.error(
              "file: App.js ~ line 34 ~ firebase.auth ~ error",
              error
            );
          });
      } else {
        console.log("aucun user");
      }
    });
  }

  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen
          name="PresentationLogo"
          component={PresentationLogoScreen}
        />
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="ResetPassword" component={ResetPasswordScreen} />
        {user && (
          <>
            <Stack.Screen name="Home" component={HomeMapScreen} />
            <Stack.Screen name="Profile" component={ProfileScreen} />
          </>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
}
