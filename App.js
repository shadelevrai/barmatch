import { StatusBar } from "expo-status-bar";
import React from "react";
import { SafeAreaView } from "react-native-safe-area-context";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

import NavigatorComponent from "./Components/Navigator";
import { View } from "react-native";

export default function App() {

  return (
    <>
      <StatusBar />
      <SafeAreaView style={{flex:1}}>
        <NavigatorComponent />
        {/* <View style={{flex}}>

        <View style={{backgroundColor:"red",width:50,height:50}}></View>
        </View> */}
      </SafeAreaView>
    </>
  );
}
