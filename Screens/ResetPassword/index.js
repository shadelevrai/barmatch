import React, { useState } from "react"
import { Text, View } from "react-native"
import {firebase} from "../../firebaseConfig"
import { TextInput } from "react-native-gesture-handler";

export default function ResetPasswordScreen({navigation}){
    const [mail,setMail]=useState(false)
    const [showConfirmationSend,setShowConfirmationSend]=useState(false)

    function ResetPassword(){
        firebase
        .auth()
        .sendPasswordResetEmail(mail)
        .then(() => setShowConfirmationSend(true))
        .catch((error) => alert(error));
      }
    
    return(
        <View>
            <Text>Reinitialisé son mdp</Text>
            <TextInput placeholder="Votre adresse mail" onChange={(text)=>setMail(text)} value={mail}></TextInput>
            {mail && <Text onPress={()=>ResetPassword()}>Envoyer moi un lien de réinitialisation</Text>}
            {showConfirmationSend && <Text>Regardez dans votre boile mail pour le lien</Text>}
            <Text onPress={()=>navigation.goBack()}>Retour</Text>
        </View>
    )
}