import React from "react";
import { Text, View } from "react-native";
import { firebase } from "../../firebaseConfig";

export default function HomeMapScreen({ navigation }) {
  return (
    <View>
      <View style={{flex:1}}>
        <Text>home</Text>
        <Text>hello {firebase.auth().currentUser.email} </Text>
        <Text onPress={() => navigation.navigate("Profile")}>
          go to profile
        </Text>
      </View>
      <View style={{flex:2}}>

      </View>
    </View>
  );
}
