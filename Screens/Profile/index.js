import React from "react"
import {Text, View} from "react-native"
import {firebase} from "../../firebaseConfig"

export default function ProfileScreen({navigation}){
    return(
        <View>
            <Text onPress={()=>firebase.auth().signOut().then(()=>navigation.navigate("Login"))}>Log out</Text>
        </View>
    )
}