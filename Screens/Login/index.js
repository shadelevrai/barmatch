import React,{useState} from "react"
import { Text, View, TextInput, TouchableOpacity } from "react-native"
import {firebase} from "../../firebaseConfig"

export default function LoginScreen({navigation}){
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    function onFooterLinkPress(){
        navigation.navigate("Registration");
      };

    function onLoginPress(){
        firebase
          .auth()
          .signInWithEmailAndPassword(email, password)
          .then((response) => {
            const uid = response.user.uid;
            const usersRef = firebase.firestore().collection("users");
            usersRef
              .doc(uid)
              .get()
              .then((firestoreDocument) => {
                if (!firestoreDocument.exists) {
                  alert("Cet utilisateur n'existe paaas");
                  return;
                }
                const user = firestoreDocument.data();
                navigation.navigate("Home", { user });
              })
              .catch((error) => {
                alert(error);
              });
          })
          .catch((error) => {
            alert(error);
          });
      };

      function goScreenResetPassword(){
        navigation.navigate("ResetPassword");
      }

    return(
        <View>
      <Text>Login</Text>
      <TextInput
        placeholder="Email"
        onChangeText={(text) => setEmail(text)}
        value={email}
        autoCapitalize="none"
      />
      <TextInput
        placeholderTextColor="#aaaaaa"
        secureTextEntry
        placeholder="Password"
        onChangeText={(text) => setPassword(text)}
        value={password}
        autoCapitalize="none"
      />
      <TouchableOpacity onPress={() => onLoginPress()}>
        <Text>Log in</Text>
      </TouchableOpacity>
      <View>
          <Text onPress={()=>goScreenResetPassword()}>Reinitialisé son mdpasse</Text>
      </View>
      <View>
        <Text>
          Don't have an account?{" "}
          <Text onPress={onFooterLinkPress}>
            Sign up
          </Text>
        </Text>
      </View>
    </View>
    )
}