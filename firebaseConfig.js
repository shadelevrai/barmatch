import firebase from 'firebase';
import '@firebase/auth';
import '@firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyAHu1xF0s9c8GkrPOEYrgAz_b7vdNg8TmY",
    authDomain: "barmatch-82b5a.firebaseapp.com",
    projectId: "barmatch-82b5a",
    storageBucket: "barmatch-82b5a.appspot.com",
    messagingSenderId: "769140219249",
    appId: "1:769140219249:web:95e443a02620d8ad19ead0",
    measurementId: "G-77GWE5M33T"
};

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}

export { firebase };